<?php
$numFibonnacci = 1;
$numeroUno = 0;
$numeroDos = 1;
$fibonacci = [$numeroUno,$numeroDos];

if (isset($_GET['num_fibonacci'])) {

    $numFibonnacci = $_GET['num_fibonacci'];

    for($i=2;$i<=$numFibonnacci;$i++)
    {
        $fibonacci[] = $fibonacci[$i-1]+$fibonacci[$i-2];
    }

}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>Indice n: <?php echo $numFibonnacci ?></h2>

    <div>
        <h2>Resultado : <?php echo $fibonacci[$numFibonnacci];?></h2>
    </div>
    <hr>
</body>
</html>